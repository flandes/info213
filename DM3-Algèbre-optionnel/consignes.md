Bonjour les L2 !

Le DM est optionnel, c.a.d. que la note ne comptera que si elle fait augmenter votre moyenne de CC.

Le DM consiste à compléter deux fichiers:
- DM3-optionel-partie1-jeux-elementaires.ipynb
- DM3-optionel-partie2-densité des matrices inversibles.ipynb
Ils sont maintenant sur le gitlab du cours. Ignorez l'ancien DM, dont le corrigé est pour partie en ligne (!)

Le DM est à rendre par email (à moi), en joignant dans un zip:
- vos deux fichiers .ipynb (si vous travaillez en ligne, exportez les, ne m'envoyez pas une page web !!)
- la version pdf de ces 2 fichiers (Fichier>Exporter>pdf)
le tout dans un zip pour éviter les antivirus trop zélés.
N'oubliez pas de mettre votre nom !

Il est à rendre avant le 3 décembre 2022, 22h00 (oui, vous avez du temps, mais il y a du boulot) !


