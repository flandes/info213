Allez voir la page 9-10 du poly:

2.2.2
Astuces pratiques (jupyter)
Astuces pour avoir accès à un jupyter-notebook qui marche:
• La Methode par défaut:
– installer python3 sur votre ordinateur (si il n’y est pas déjà)
– installer jupyter-notebook sur votre ordinateur
– installer les librairies de base, en particulier numpy et matplotlib (éventuelle-
ment seaborn ou sklearn si ça vous amuse)
– télécharger les énoncés, et lancer les premières cellules des notebook. Si il vous
manque des packages, vous aurez des erreurs relatives à ça..
– une alternative est de se connecter en ssh sur les ordis de la fac... mais vu la
facilité avec laquelle on installe python, je recommande plutot d’installer python
(+jupyter+les packages de base)
– dans la mesure du possible, avoir un moyen de partager votre écran où discuter
facilement avec votre binôme - par exemple en utilisant un discord (cf plus bas)
• Alternative 1: https://jupyterhub.ijclab.in2p3.fr/ (ça marche mieux sous
cette nouvelle instance ! Il y a plus de coeurs de calcul !)
9– Avec vos identifiants paris-saclay, vous devriez avoir une session sur le cloud du
LAL (Paris Saclay)
– dans votre dossier ‘work‘ (ou un autre que vous créez), importez le fichier .ipynb
de votre choix (cliquer sur le bouton "upload", en haut à droite)
– travaillez, directement en ligne
• Alternative 2: (moins bien, google..)
– Il vous faudra un compte google malheureusement pour l’utiliser
– Aller sur https://colab.research.google.com/notebooks/intro.ipynb
– ouvrez les notebook que vous voulez
